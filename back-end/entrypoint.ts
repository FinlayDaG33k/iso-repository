import { Updater } from "./src/updater.ts";
import { Webserver } from "./lib/webserver/webserver.ts";
import { Logger } from "./lib/logging.ts";
import { cron } from "./lib/common/cron.ts";


const updater = new Updater();
await updater.update();
cron('* * * * *', async () => { await updater.update(); });

// Load routes
import "./src/routes.ts";

// Start webserver
Logger.info(`Starting webserver on port "8080"...`);
const server = new Webserver(8080);
server.start();
Logger.info(`Webserver server started!`);
