import { torrents } from "../torrents.ts";

export function findInfoHash(hash: string) {
  let foundTorrent = torrents.find((torrent: any) => torrent.info_hash === hash);
  if (foundTorrent !== undefined) return foundTorrent;
  return null;
}
