import { encode, decode } from "https://deno.land/x/bencode@v0.1.3/mod.ts";
import { createHash } from "https://deno.land/std@0.107.0/hash/mod.ts";
import { decode as hexDecode } from "https://deno.land/std@0.107.0/encoding/hex.ts";

interface ITorrent {
  info_hash: string;
  name: string;
  family: string;
  creation_date: number;
  size: number;
  files: IFile[];
  peers: {
    seeders: number;
    leechers: number;
  }
}

interface IFile {
  name: string;
  size: number;
}

export class Torrent {
  private readonly dir: string;
  private readonly name: string;
  private readonly family: string;
  private torrent: any = {};
  private trackers: string[] = [];
  private info_hash: string = '';
  private size: number = 0;
  private files: IFile[] = [];
  private peers: any = {
    seeders: 0,
    leechers: 0
  };

  public constructor(dir: string, name: string, family: string = 'Generic ISO') {
    this.dir = dir;
    this.name = name.replace(".torrent", "");
    this.family = family;
  }

  public getCreationDate(): number {
    return this.torrent['creation date'];
  }

  public getInfohash(): string {
    // Check if we already calculated the info_hash
    if(this.info_hash !== '') return this.info_hash;

    // Calculate the info_hash
    const encoded = encode(this.torrent.info);
    const hash = createHash("sha1");
    hash.update(encoded);
    this.info_hash = hash.toString();

    return this.info_hash;
  }

  public async scrape() {
    const bytes = hexDecode(new TextEncoder().encode(this.info_hash));
    // @ts-ignore
    const encoded = escape(String.fromCharCode(...bytes));

    // Scrape the tracker
    try {
      const resp = await fetch(`https://tracker.finlaydag33k.nl/scrape?info_hash=${encoded}`);
      if(resp.status !== 200) {
        this.peers.seeders = 0;
        this.peers.leechers = 0;
      }

      const body = await resp.arrayBuffer();
      const data = decode(body) as any;
      const key = Object.keys(data['files'])[0];
      if(data['files'][key]['incomplete']) this.peers.leechers = data['files'][key]['incomplete'];
      if(data['files'][key]['complete']) this.peers.seeders = data['files'][key]['complete'];
    } catch(e) {
      console.log(e);
    }
  }

  public getData(): ITorrent {
    return {
      info_hash: this.getInfohash(),
      name: this.name,
      family: this.family,
      magnet: this.getMagnet(),
      creation_date: this.getCreationDate(),
      size: this.size,
      files: this.files,
      peers: {
        seeders: this.peers.seeders,
        leechers: this.peers.leechers
      }
    } as ITorrent;
  }

  public async parse() {
    // Load the main content
    const content = await Deno.readFile(`${this.dir}/${this.name}.torrent`);
    this.torrent = decode(content);

    for(const file of this.torrent.info.files) {
      this.size += file.length;
      this.files.push({
        name: file.path[0],
        size: file.length
      });
    }

    // Load a list of all trackers
    // If none found, just use the default announce
    if(Array.isArray(this.torrent['announce-list'])) {
      for(const group of this.torrent['announce-list']) {
        for(const tracker of group) {
          if(tracker === "dht:") continue;
          this.trackers.push(tracker);
        }
      }
    } else {
      this.trackers.push(this.torrent['announce']);
    }

    // Get the infohash
    this.getInfohash();

    // Scrape the tracker
    await this.scrape();
  }

  public getMagnet(): string {
    let magnet = 'magnet:?xt=urn:btih:';

    // Add the infohash
    if(this.getInfohash()) magnet += this.getInfohash();

    // Add file name
    if(this.name) magnet += `&dn=${this.name}`;

    // Add trackers
    for(const tracker of this.trackers) {
      magnet += `&tr=${encodeURIComponent(tracker)}`;
    }

    return magnet;
  }
}
