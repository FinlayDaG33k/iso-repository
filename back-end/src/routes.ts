import { Router } from "../lib/webserver/routing/router.ts";

Router.add({path: '/repository', controller: 'Torrents', action: 'index', method: 'GET'});
Router.add({path: '/repository/:dir', controller: 'Torrents', action: 'index', method: 'GET'});
Router.add({path: '/torrent/:info_hash', controller: 'Torrents', action: 'view', method: 'GET'});
Router.add({path: '/latest', controller: 'Torrents', action: 'latest', method: 'GET'});
Router.add({path: '/popular', controller: 'Torrents', action: 'popular', method: 'GET'});
