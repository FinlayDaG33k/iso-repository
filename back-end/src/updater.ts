import { Torrent } from './torrent.ts';
import { torrents, setTorrents } from './torrents.ts';

export class Updater {
  private isUpdating: boolean = false;

  public async update() {
    // Make sure we are not updating already
    if(this.isUpdating) return;

    // Mark as updating
    this.isUpdating = true;

    // Update
    await this.processDir(`${Deno.cwd()}/../data/torrents`);

    // Mark as finished
    this.isUpdating = false;
  }

  private async processDir(dir: string) {
    for await (const dirEntry of Deno.readDir(dir)) {
      if(dirEntry.isDirectory) this.processDir(`${dir}/${dirEntry.name}`);
      if(dirEntry.isFile) this.processTorrent(dir, dirEntry.name);
    }
  }

  private async processTorrent(dir: string, name: string): Promise<void> {
    const family = dir.replace(`${Deno.cwd()}/../data/torrents/`, "");
    const torrent = new Torrent(dir, name, family);
    await torrent.parse();

    // Check if our torrent already exists
    // If not, add it
    const existing = torrents.find((entry: any) => entry.info_hash === torrent.getData().info_hash);
    if(!existing) {
      console.log(`Found new torrent: ${torrent.getData().name} (${torrent.getData().info_hash})`);
      torrents.push(torrent.getData());
    } else {
      existing.peers.seeders = torrent.getData().peers.seeders;
      existing.peers.leechers = torrent.getData().peers.leechers;
    }
  }
}
