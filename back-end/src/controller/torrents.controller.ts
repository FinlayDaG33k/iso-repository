import { Controller } from "../../lib/webserver/controller/controller.ts";
import { RouteArgs } from "../../lib/webserver/routing/router.ts";
import { torrents } from "../torrents.ts";
import { findInfoHash } from "../util/find-infohash.ts";

export class TorrentsController extends Controller {
  public async index(args: RouteArgs) {
    if(!args.params.hasOwnProperty('dir')) {
      // Get a list of all families
      let families = [...new Set(torrents.map((torrent: any) => torrent.family))];

      // Loop over each family to get some meta
      let result: any = {};
      families.forEach((family: string) => {
        let entries = torrents.filter((torrent: any) => torrent.family === family)
        result[family] = {
          count: entries.length,
          last_update: entries.reduce((a: any,b: any)=>a.creation_date>b.creation_date?a:b).creation_date
        }
      });

      super.set('data', { result: result});
      super.type = 'application/json';
      return;
    }

    const family = args.params.dir.replace("%20", " ");
    let entries = torrents.filter((torrent: any) => torrent.family === family)
    super.set('data', { result: entries });
    super.type = 'application/json';
  }

  public async view(args: RouteArgs) {
    // Find our torrent in the list
    const res = findInfoHash(args.params.info_hash);

    // Check if a result was found
    if(!res) {
      super.set('data', { result: [] });
      super.type = 'application/json';
      return;
    }

    super.set('data', { result: res });
    super.type = 'application/json';
  }

  public async latest(args: RouteArgs) {
    // Sort the torrents list
    const sorted = torrents.sort(function(a: any, b: any) {
      if (a.creation_date === b.creation_date) return 0;
      if (a.creation_date < b.creation_date) return 1;
      return -1;
    });

    // Get the first 10 results
    let result = [];
    if(sorted.length <= 10) {
      result = sorted;
    } else {
      result = sorted.slice(0, 10);
    }

    super.set('data', { result: result });
    super.type = 'application/json';
  }

  public async popular(args: RouteArgs) {
    // Sort the torrents list
    const sorted = torrents.sort(function(a: any, b: any) {
      const a_peers = a.peers.seeders + a.peers.leechers;
      const b_peers = b.peers.seeders + b.peers.leechers;
      if (a_peers === b_peers) return 0;
      if (a_peers < b_peers) return 1;
      return -1;
    });

    // Get the first 10 results
    let result = [];
    if(sorted.length <= 10) {
      result = sorted;
    } else {
      result = sorted.slice(0, 10);
    }

    super.set('data', { result: result });
    super.type = 'application/json';
  }
}
