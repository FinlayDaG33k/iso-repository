import Component from "@glimmer/component";

export default class ListGroupFileComponent extends Component {
  name = '';
  size = 0;

  constructor(...args) {
    super(...args);

    this.name = this.args.name;
    this.size = this.args.size ?? 0;
  }
}
