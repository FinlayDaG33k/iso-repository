import Component from "@glimmer/component";
import { inject as service } from '@ember/service';
import {tracked} from "@glimmer/tracking";
import { A } from '@ember/array';
import ENV from 'front-end/config/environment';

export default class LatestTorrentsComponent extends Component {
  @service torrents;
  @tracked items = A([]);

  constructor(...args) {
    super(...args);

    this.load();
  }

  async load() {
    // Get all latest torrents
    // TODO: Implement cache
    let resp = await fetch(`${ENV.APP.REPO_HOST}/latest`);
    let data = await resp.json();

    // Add torrents to store
    for(const torrent of data.result) {
      if(!this.torrents.find(torrent.info_hash)) this.torrents.add(torrent);
    }

    this.items = A(data.result);
  }
}
