import Component from "@glimmer/component";

export default class ListGroupTorrentComponent extends Component {
  torrent = {};

  constructor(...args) {
    super(...args);

    this.torrent = this.args.torrent;

    this.name = this.args.name;
    this.seeders = this.args.seeders;
    this.leechers = this.args.leechers;
    this.creation_date = this.args.creation_date;
    this.info_hash = this.args.info_hash;
  }
}
