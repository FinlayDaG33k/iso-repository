import Component from "@glimmer/component";

export default class ListGroupIndexComponent extends Component {
  family = '';
  count = 0;
  last_update = 0;

  constructor(...args) {
    super(...args);

    this.family = this.args.family;
    this.count = this.args.data.count;
    this.last_update = this.args.data.last_update;
  }
}
