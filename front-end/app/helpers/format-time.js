import { helper } from '@ember/component/helper';

const months = [
  'Jan', 'Feb', 'Mar',
  'Apr', 'Jun', 'Jul',
  'Aug', 'Sep', 'Oct',
  'Nov', 'Dec'
];

function formatTime([value]) {
  let date = new Date(value * 1000),
    year = date.getFullYear(),
    month = date.getMonth(),
    day = date.getDate(),
    min = date.getMinutes(),
    sec = date.getSeconds(),
    hour = date.getHours();

  return ('0' + day).slice(-2) + '-' +
    (months[month - 1] ?? months[month]) + '-' +
    year +
    ' ' +
    (hour < 10 ? ("0" + hour) : hour) + ":" +
    (min < 10 ? ("0" + min) : min) + ":" +
    (sec < 10 ? ("0" + sec) : sec);
}

export default helper(formatTime);
