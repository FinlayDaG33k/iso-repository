import EmberRouter from '@ember/routing/router';
import config from 'front-end/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('home', {path: '/'});
  this.route('faq', {path: '/faq'});
  this.route('donate', {path: '/donate'});
  this.route('legal', {path: '/legal'});

  this.route('family', {path:'/family/:name'});
  this.route('torrent', {path:'/torrent/:info_hash'})
});
