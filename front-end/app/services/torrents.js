import Service from '@ember/service';
import { tracked } from "@glimmer/tracking";

export default class TorrentsService extends Service {
  @tracked torrents = {};

  add(data) {
    this.torrents[data.info_hash] = data;
  }

  find(info_hash) {
    if(!this.torrents.hasOwnProperty(info_hash)) return null;
    return this.torrents[info_hash];
  }
}
