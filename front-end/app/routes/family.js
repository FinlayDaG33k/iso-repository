import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import ENV from 'front-end/config/environment';

export default class FamilyRoute extends Route {
  @service torrents;

  async model(params) {
    // Get all torrents for this family
    // TODO: Implement cache
    let resp = await fetch(`${ENV.APP.REPO_HOST}/repository/${params.name}`);
    let data = await resp.json();

    for(const torrent of data.result) {
      if(!this.torrents.find(torrent.info_hash)) this.torrents.add(torrent);
    }

    return {
      name: params.name,
      torrents: data.result
    }
  }
}
