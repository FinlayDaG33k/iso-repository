import Route from '@ember/routing/route';
import ENV from 'front-end/config/environment';

export default class HomeRoute extends Route {
  async model() {
    const resp = await fetch(`${ENV.APP.REPO_HOST}/repository`);
    const families = await resp.json();

    return {
      families: families.result
    };
  }
}
