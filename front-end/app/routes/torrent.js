import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import ENV from 'front-end/config/environment';

export default class TorrentRoute extends Route {
  @service torrents;

  async model(params) {
    // Try to find the infohash in our torrents service
    let torrent = this.torrents.find(params.info_hash);
    console.log(torrent);

    // Check if a torrent exists
    // If so, return it
    if(torrent) return torrent;

    // Try to download the torrent info from the server
    const res = await fetch(`${ENV.APP.REPO_HOST}/torrent/${params.info_hash}`);
    const data = await res.json();
    return data.result;
  }
}
